import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { Pipe, PipeTransform } from '@angular/core';

@Component({
  selector: 'app-byname',
  templateUrl: './byname.component.html'
})
export class ByNameComponent implements OnInit {

  search: string;
  title: string;
  names: any[];
  return: any[];

  /// Active filter rules
  filters = {}

  constructor(private route: ActivatedRoute, db: AngularFireDatabase) {
    if (this.route.snapshot.params['search']) {

      var routeValue = this.route.snapshot.params['search'];
      var names = db.list('/pessoas').valueChanges();
      var filtered = [];

      this.search = routeValue;
      this.title = 'Search results by name: '+ this.search;

      routeValue = routeValue.toLowerCase();
      names.forEach(obj => { obj.forEach(childObj => {
          var item = childObj;
          if( item.name.toLowerCase().indexOf(routeValue) >= 0 ) {
            filtered.push(childObj);
            this.return = filtered;
          }
          if(filtered.length == 0) {
            this.return = [{error: "Não foram encontrados registros."}];
          }
      }); });
      this.return = [{error: "Foi passado parâmetro de busca."}];
    }
    else {
      this.return = [{error: "Não foi passado parâmetro de busca."}];
      this.route.data.subscribe(data=> console.log(data));
    }

  }

  ngOnInit() { }
}