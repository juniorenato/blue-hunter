import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ByNameComponent } from './byname.component';

describe('ByNameComponent', () => {
  let component: ByNameComponent;
  let fixture: ComponentFixture<ByNameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ByNameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ByNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
