import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ByEmailComponent } from './byemail.component';

describe('ByEmailComponent', () => {
  let component: ByEmailComponent;
  let fixture: ComponentFixture<ByEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ByEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ByEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
