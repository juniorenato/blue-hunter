import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserComponent } from './user/user.component';
import { ByNameComponent } from './user/by-name/byname.component';

const APP_ROUTES: Routes = [
    { path: 'user', component: UserComponent },
    { path: 'user/by-name/:search', component: ByNameComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);