import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { NgForm, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { Router, Routes, RouterModule } from '@angular/router';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html'
})

export class UserComponent implements OnInit {

    constructor(private router:Router) { }

    ngOnInit() { }

    onSubmit = function(search) {
        this.router.navigate(["/user/"+ search.by.toLowerCase() +"/"+ search.search.toLowerCase()]);
    }

}
