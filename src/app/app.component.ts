import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { NgForm, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { Router, Routes, RouterModule } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

    constructor(private router:Router) { }

    title = 'Search';

    ngOnInit() { }

    onSubmit = function(search) {
        this.router.navigate(["/"+ search.type.toLowerCase() +"/by-name/"+ search.search.toLowerCase()]);
    }

}
